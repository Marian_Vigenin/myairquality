#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <iostream>
#include <ctime>

#include "MeasurementReader.h"
#include "SerialPortSetup.h"
#include "DataBaseService.h"

/*
 * This implementation can be used in combination with curl to pass the measurement
 * to external service.
 * i.e.
 * ./myairquality 2 | curl -k --data-binary @- -H "Content-Type:application/json" https://postman-echo.com/post --verbose
 * 
 * example for loop execution in bash:
 * for (( ; ; )); do    ./myairquality 2;    sleep 60; done
 * 
 * sqlite3 db file is located at: /var/log/myair/myair.db
 * log file is located at: /var/log/myair/airdata.txt
 * port name in BBB is: /dev/ttyS4
 * 
 * sqlite3 myair.db structure:
 * CREATE TABLE myairdata (measure_id INTEGER PRIMARY KEY AUTOINCREMENT, device INTEGER, co2 REAL, voc REAL, hum REAL, temp REAL, pm25 REAL, time INTEGER);
 *  
 */

int main(int argc, char* argv[])
{
    const char *portname = "/dev/ttyS4";
    int commandType;
    unsigned int deviceId = 100;
           
    if(argc > 1){
      commandType = atoi(argv[1]);    

    }
    SerialPortSetup serialPort = SerialPortSetup(portname,'N');
    MeasurementReader reader = MeasurementReader();
    MeasurementReader::MeasurementData data = reader.readAll(&serialPort);
    data.setDeviceId(deviceId);
    char time[256] = "";
    {
      struct tm *tmp;
      time_t t = std::time(0);
      tmp = localtime(&t);
      strftime(time, 256, "%F %H:%M:%S", tmp);
    }

    switch (commandType){
            case 1: {
                // console output
                printf("CO2: %.2f ppm\n",     data.getCo2());
                printf("VOC: %.2f ppm\n",     data.getVoc());
                printf("HUM: %.2f %%\n",      data.getHumidity());
                printf("Temp: %.2f C\n",      data.getTemp());
                printf("PM2.5: %.2f ug/m3\n", data.getPm25());
                printf("Time: %s\n", time);
                break;
            }
            case 2: {
                // JSON mode as output. Can be used with pipe to curl
                printf("{\"CO2\":\"%.2f\",\"VOC\":\"%.2f\",\"HUM\":\"%.2f\",\"Temp\":\"%.2f\",\"PM2.5\":\"%.2f\",\"Time\":\"%s\"}\n", 
                        data.getCo2(), data.getVoc(), data.getHumidity(), data.getTemp(), data.getPm25(), time);
                break;
            }

            case 3: {
                // save data into DB
                DataBaseService dbService = DataBaseService();
                dbService.saveMeasurement("/var/log/myair/myair.db", &data);
                break;
            }
            default : {
                // save data to file
                FILE * pFileTXT = fopen ("/var/log/myair/airdata.txt","a");
                fprintf (pFileTXT, "{\"CO2\":\"%.2f\",\"VOC\":\"%.2f\",\"HUM\":\"%.2f\",\"Temp\":\"%.2f\",\"PM2.5\":\"%.2f\",\"Time\":\"%s\"}\n", 
                data.getCo2(), data.getVoc(), data.getHumidity(), data.getTemp(), data.getPm25(), time);
                fclose(pFileTXT);
                break;
            }
    }    
}