/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataBaseService.h
 * Author: mvigenin
 *
 * Created on 12 January 2020, 12:31
 * 
 * This class is supposed to work with SqLite database.
 * 
 * The table structure is the following:
 * 
 * CREATE TABLE myairdata (measure_id INTEGER PRIMARY KEY AUTOINCREMENT, device INTEGER, co2 REAL, voc REAL, hum REAL, temp REAL, pm25 REAL, time INTEGER);
 *
 * Sample row data:
 * {"DID":"BBB1",CO2":"1397.00","VOC":"0.07","HUM":"42.40","Temp":"23.70","PM2.5":"4.00"}
 */

#ifndef DATABASESERVICE_H
#define DATABASESERVICE_H

#include "MeasurementReader.h"

class DataBaseService {
public:
    DataBaseService();
    DataBaseService(const DataBaseService& orig);
    virtual ~DataBaseService();
    int saveMeasurement(const char* dbName, MeasurementReader::MeasurementData* data);
    static int dbCallback(void *NotUsed, int argc, char **argv, char **azColName);
private:

};

#endif /* DATABASESERVICE_H */

