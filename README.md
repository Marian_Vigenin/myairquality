### Installing the application as a service in BBB ###
- copy file myair.service to /etc/systemd/system
- systemctl daemon-reload
- systemctl enable myair.service
- systemctl start myair.service

### Configurations: ###
 This implementation is based on ZH03 air quality sensor (https://www.winsen-sensor.com/sensors/dust-sensor/zh3b.html)  
 It is using serial port for reading the measurements. Reading logic is in "MeasurementReader.cpp".  
 In case of using other sensors, just replace that implementation with your own.  
 Log file is created at: /var/log/myair/airdata.txt  
 Port name in BBB is: /dev/ttyS4  
 Measurements are taken once a minute (check myair.sh for sample code)  

### DB structure: check dbstructure.sql ###
 sqlite3 db file is created at: /var/log/myair/myair.db  
 sqlite3 myair.db structure:  
 CREATE TABLE myairdata (measure_id INTEGER PRIMARY KEY AUTOINCREMENT, device INTEGER, co2 REAL, voc REAL, hum REAL, temp REAL, pm25 REAL, time INTEGER);  

### Installing the UI part: ###
- install apache2
- install php
- install sqlite3 module for php
- copy the contents of UI folder to /var/www/html (or the document folder of your site, as this is the default one)
- update you locale in config.php
- update the DB file location in config.php, if it different than the default /var/log/myair/myair.db.

### Usage: ###
http://<your-device-ip>:8080/index.php?period=hour

[Example view](myairquality.jpg)