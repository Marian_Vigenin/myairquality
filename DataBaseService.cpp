/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataBaseService.cpp
 * Author: mvigenin
 * 
 * Created on 12 January 2020, 12:31
 */

#include "DataBaseService.h"
#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h> 

DataBaseService::DataBaseService() {
}

DataBaseService::DataBaseService(const DataBaseService& orig) {
}

DataBaseService::~DataBaseService() {
}

int DataBaseService::saveMeasurement(const char* dbName, MeasurementReader::MeasurementData* data){
   sqlite3 *db;
   char *zErrMsg = 0;
   int rc;
   char* sql = (char *) malloc( 1024 * sizeof( char ) );

   rc = sqlite3_open(dbName, &db);

   if( rc ) {
      fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
      return(0);
   }
#ifdef DEBUG_OUTPUT    
   else {
      fprintf(stderr, "Opened database successfully\n");
   }
#endif
   
   snprintf(sql, 1024, "INSERT INTO myairdata (device,co2,voc,hum,temp,pm25,time) VALUES (%i, %.2f, %.2f, %.2f, %.2f, %.2f, %i );",
            data->getDeviceId(), data->getCo2(), data->getVoc(), data->getHumidity(), data->getTemp(), data->getPm25(), data->getTimestamp());
   
      /* Execute SQL statement */
   rc = sqlite3_exec(db, sql, DataBaseService::dbCallback, 0, &zErrMsg);
   
   if( rc != SQLITE_OK ){
      fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
   } 
#ifdef DEBUG_OUTPUT    
   else {
      fprintf(stdout, "Records created successfully\n");
   }
#endif   
   
   sqlite3_close(db);
   
   free(sql);
   
   return 0;
} 

int DataBaseService::dbCallback(void *NotUsed, int argc, char **argv, char **azColName) {
   int i;
   for(i = 0; i<argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}

