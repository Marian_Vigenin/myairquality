<?php
header("Access-Control-Allow-Origin: *");
header("Content-type:application/json;charset=utf-8");

require("./config.php");

$period = $_GET['period'];

$timeAdjust = "";

switch($period){
  case "hour" : $timeAdjust = "CAST(strftime('%s','now') AS integer) - 3600"; break;
  case "2hours" : $timeAdjust = "CAST(strftime('%s','now') AS integer) - 7200"; break;
  case "3hours" : $timeAdjust = "CAST(strftime('%s','now') AS integer) - 10800"; break;
  case "today" : $timeAdjust = "CAST(strftime('%s',date('now','start of day')) AS integer)"; break;
  case "month" : $timeAdjust = "CAST(strftime('%s',date('now','start of month')) AS integer)"; break;
  default: $timeAdjust = "CAST(strftime('%s','now') AS integer) - 3600";
}

$db = new SQLite3($config['db']['file']);
//'start of day'
$res = $db->query("SELECT * FROM myairdata WHERE time > $timeAdjust");

$allRows=array();
while ($row = $res->fetchArray(SQLITE3_ASSOC)) {
  array_push($allRows,$row);
}

echo json_encode($allRows);
?>