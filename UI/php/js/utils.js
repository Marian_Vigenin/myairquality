'use strict';

window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

(function(global) {
    var MONTHS = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];

    var COLORS = [
        '#4dc9f6',
        '#f67019',
        '#f53794',
        '#537bc4',
        '#acc236',
        '#166a8f',
        '#00a950',
        '#58595b',
        '#8549ba'
    ];

    var Samples = global.Samples || (global.Samples = {});
    var Color = global.Color;

    Samples.utils = {
        // Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
        srand: function(seed) {
            this._seed = seed;
        },

        rand: function(min, max) {
            var seed = this._seed;
            min = min === undefined ? 0 : min;
            max = max === undefined ? 1 : max;
            this._seed = (seed * 9301 + 49297) % 233280;
            return min + (this._seed / 233280) * (max - min);
        },

        numbers: function(config) {
            var cfg = config || {};
            var min = cfg.min || 0;
            var max = cfg.max || 1;
            var from = cfg.from || [];
            var count = cfg.count || 8;
            var decimals = cfg.decimals || 8;
            var continuity = cfg.continuity || 1;
            var dfactor = Math.pow(10, decimals) || 0;
            var data = [];
            var i, value;

            for (i = 0; i < count; ++i) {
                value = (from[i] || 0) + this.rand(min, max);
                if (this.rand() <= continuity) {
                    data.push(Math.round(dfactor * value) / dfactor);
                } else {
                    data.push(null);
                }
            }

            return data;
        },

        labels: function(config) {
            var cfg = config || {};
            var min = cfg.min || 0;
            var max = cfg.max || 100;
            var count = cfg.count || 8;
            var step = (max - min) / count;
            var decimals = cfg.decimals || 8;
            var dfactor = Math.pow(10, decimals) || 0;
            var prefix = cfg.prefix || '';
            var values = [];
            var i;

            for (i = min; i < max; i += step) {
                values.push(prefix + Math.round(dfactor * i) / dfactor);
            }

            return values;
        },

        months: function(config) {
            var cfg = config || {};
            var count = cfg.count || 12;
            var section = cfg.section;
            var values = [];
            var i, value;

            for (i = 0; i < count; ++i) {
                value = MONTHS[Math.ceil(i) % 12];
                values.push(value.substring(0, section));
            }

            return values;
        },

        color: function(index) {
            return COLORS[index % COLORS.length];
        },

        transparentize: function(color, opacity) {
            var alpha = opacity === undefined ? 0.5 : 1 - opacity;
            return Color(color).alpha(alpha).rgbString();
        }
    };

    // DEPRECATED
    window.randomScalingFactor = function() {
        return Math.round(Samples.utils.rand(-100, 100));
    };

    // INITIALIZATION

    Samples.utils.srand(Date.now());

    // Google Analytics
    /* eslint-disable */
    if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-28909194-3', 'auto');
        ga('send', 'pageview');
    }
    /* eslint-enable */

}(this));


    /* custom chart helper methods and vars */
    var timeDataSet = new Array();
    var tempDataSet = new Array();
    var humDataSet = new Array();
    var pm25DataSet = new Array();
    var vocDataSet = new Array();
    var co2DataSet = new Array();
    var serviceUrl = "";//"http://192.168.0.105:8080/data.php?period=";
    var systemLocale = ""; //"bg-BG";

    window.dataCharts = new Map();

    function buildChart(canvas, label, dataSet, xAxisLabels, color) {
        let chartData = {
            labels: xAxisLabels,
            datasets: [{
                label: label,
                borderColor: color,
                backgroundColor: color,
                fill: false,
                data: dataSet,
                yAxisID: 'y-axis-1',
            }]
        };

        let ctx = document.getElementById(canvas).getContext('2d');
        let aChart = Chart.Line(ctx, {
            data: chartData,
            options: {
                responsive: true,
                //hoverMode: 'index',
                stacked: false,
                title: {
                    display: true,
                    text: label + ' chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    yAxes: [{
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'left',
                        id: 'y-axis-1'
                    }]
                }
            }
        });

        window.dataCharts.set(canvas, aChart);
    }

    function updateDataSets(data) {
        timeDataSet.length = 0;
        tempDataSet.length = 0;
        humDataSet.length = 0;
        pm25DataSet.length = 0;
        vocDataSet.length = 0;
        co2DataSet.length = 0;
        data.forEach(function (it, index) {
            timeDataSet.push(new Date(it.time * 1000).toLocaleTimeString(systemLocale));
            // level the wrong measurements
            if (it.temp == -50 && index > 0) {
                tempDataSet.push(tempDataSet[index - 1]);
            } else {
                tempDataSet.push(it.temp);
            }

            if (it.hum == 0 && index > 0) {
                humDataSet.push(humDataSet[index - 1]);
            } else {
                humDataSet.push(it.hum);
            }

            pm25DataSet.push(it.pm25);

            vocDataSet.push(it.voc)

            if ((it.co2 < 1 || it.co2 > 1999) && index > 0) {
                co2DataSet.push(co2DataSet[index - 1]);
            } else {
                co2DataSet.push(it.co2);
            }
        })
    }

    function buildAllCharts(data, status) {
        updateDataSets(data);
        buildChart('temperature', 'Temperature', tempDataSet, timeDataSet, window.chartColors.red);
        buildChart('humidity', 'Humidity', humDataSet, timeDataSet, window.chartColors.blue);
        buildChart('pm25', 'PM2.5', pm25DataSet, timeDataSet, window.chartColors.green);
        buildChart('voc', 'VOC', vocDataSet, timeDataSet, window.chartColors.purple);
        buildChart('co2', 'CO2', co2DataSet, timeDataSet, window.chartColors.yellow);
    }

    function updateAllCharts(data, status) {
        updateDataSets(data);
        window.dataCharts.forEach(function(value, key, map){
            value.update();
        })
    }

    function refresh(){
        $.get(serviceUrl+document.getElementById('period').value, updateAllCharts);
    }

    $( document ).ready(function () {
        $.get(serviceUrl+"hour", function(data,status){
            buildAllCharts(data, status)
        });

        document.getElementById('period').addEventListener('change', function () {
            $.get(serviceUrl+this.value, updateAllCharts)
        });

        window.setInterval(refresh, 1000*60*5);

    });

