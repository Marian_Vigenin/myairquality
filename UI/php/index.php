<!doctype html>
<html>

<head>
    <title>My air quality charts</title>
    <link rel="stylesheet" href="css/styles.css"></style>
    <script src="js/jquery.min.js"></script>
    <script src="js/Chart.min.js"></script>
    <script src="js/utils.js"></script>
</head>

<body>
<?php require("./config.php"); ?>
<script>
    serviceUrl = "<?= $config['dataservice']['url'] ?>";
    systemLocale = "<?= $config['date']['timezone'] ?>";
</script>
<div class="period-selector">
    <select id="period">
        <option value="hour" selected>Last hour</option>
        <option value="2hours">Last 2 hours</option>
        <option value="3hours">Last 3 hours</option>
        <option value="today">Today</option>
<!--    <option value="week">Last week</option>-->
        <option value="month">Last month</option>
<!--    <option value="dateRange">Select date range</option>-->
    </select>
</div>

<div class="chart-wrapper">
    <canvas id="temperature"></canvas>
</div>

<div class="chart-wrapper">
    <canvas id="humidity"></canvas>
</div>

<div class="chart-wrapper">
    <canvas id="pm25"></canvas>
</div>

<div class="chart-wrapper">
    <canvas id="voc"></canvas>
</div>

<div class="chart-wrapper">
    <canvas id="co2"></canvas>
</div>

</body>

</html>

