/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SerialPortSetup.h
 * Author: mvigenin
 *
 * Created on 03 January 2020, 18:56
 */

#ifndef SERIALPORTSETUP_H
#define SERIALPORTSETUP_H

class SerialPortSetup {
public:
    SerialPortSetup(const char *portName, char connectionType);
    SerialPortSetup(const SerialPortSetup& orig);
    virtual ~SerialPortSetup();
    int getPortDescriptor();
    int closePortDescriptor();
    const char * getPortName();
private:
    const char *portName;
    char connectionType;
    int portDescriptor;
    void setMincount(int fd, int mcount);
    int setCanonicalInterfaceAttribs(int fd, int speed);
    int setInterfaceAttribs(int fd, int speed);
};

#endif /* SERIALPORTSETUP_H */

