/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MeasurementReader.h
 * Author: mvigenin
 *
 * Created on 03 January 2020, 18:50
 */

#ifndef MEASUREMENTREADER_H
#define MEASUREMENTREADER_H

#include "SerialPortSetup.h"

class MeasurementReader {
public:
    MeasurementReader();
    MeasurementReader(const MeasurementReader& orig);
    virtual ~MeasurementReader();
    
    float readCo2(unsigned char* response, int responseLength);
    float readVoc(unsigned char* response, int responseLength);
    float readTemp(unsigned char* response, int responseLength);
    float readHumidity(unsigned char* response, int responseLength);
    float readPm25(unsigned char* response, int responseLength);
    bool isValidResponse(unsigned char* response, int responseLength);
    
    
    class MeasurementData {
        private:
            unsigned int deviceId;
            float co2;
            float voc;
            float temp;
            float humidity;
            float pm25;
            long  timestamp;
        public:
            MeasurementData(float co2, float voc, float temp, float hum, float pm25, long timestamp);
            float getCo2();
            float getVoc();
            float getTemp();
            float getHumidity();
            float getPm25();
            long getTimestamp();
            unsigned int getDeviceId();
            void setDeviceId(unsigned int id);
            
    };
    
    MeasurementReader::MeasurementData readAll(SerialPortSetup* serialPort);
private:
    unsigned char READ_ALL[5]   = {0x11,0x02,0x01,0x00,0xEC};
    unsigned char PM25_OPEN[6]  = {0x11,0x03,0x0C,0x02,0x1E,0xC0};
    unsigned char PM25_CLOSE[6] = {0x11,0x03,0x0C,0x01,0x1E,0xC1};
    /* DF1 at index 3
     * DF2 at index 4
     * CS  at index 5
     * DF1*256 + DF2 is the zero target CO2 value. CS is the checksum*/
    unsigned char CO2_CALIB[6] = {0x11,0x03,0x03,0xFF,0xFF,0xFF};
    float readMeasurement(unsigned char* response, int responseLength, int pos);

};

#endif /* MEASUREMENTREADER_H */

