/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MeasurementReader.cpp
 * Author: mvigenin
 * 
 * Created on 03 January 2020, 18:50
 */

#include "directives.h"
#include "MeasurementReader.h"
#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/time.h>


MeasurementReader::MeasurementReader() {
}

MeasurementReader::MeasurementReader(const MeasurementReader& orig) {
}

MeasurementReader::~MeasurementReader() {
}

float MeasurementReader::readMeasurement(unsigned char* response, int responseLength, int pos){
    float result = 0.0;
    if(responseLength == 14 && isValidResponse(response, responseLength)){
        result = response[pos]*256+response[pos+1];
    }
    return result;
}

float MeasurementReader::readCo2(unsigned char* response, int responseLength){
    return readMeasurement(response, responseLength, 3);
}

float MeasurementReader::readVoc(unsigned char* response, int responseLength){
    return readMeasurement(response, responseLength, 5)/100;   
}

float MeasurementReader::readTemp(unsigned char* response, int responseLength){
    return (readMeasurement(response, responseLength, 9)-500)/10;
}

float MeasurementReader::readHumidity(unsigned char* response, int responseLength){
    return readMeasurement(response, responseLength, 7)/10;    
}

float MeasurementReader::readPm25(unsigned char* response, int responseLength){
    return readMeasurement(response, responseLength, 11);    
}

bool MeasurementReader::isValidResponse(unsigned char* response, int responseLength){
    if(responseLength > 3){
        if(response[0] == 0x16 && response[1] == 0xB && response[2] == 0x1){
            int checksum = 0;
            for (int i = 0; i < responseLength-1; i++){
                checksum += response[i];        
            }
               //modulo 256 sum
            checksum %= 256;
            char ch = checksum;

            //twos complement
            unsigned char twoscompl = ~ch + 1;
            
            if(twoscompl == response[13]){
                return true;
            }
        }
        return false;
    }
    return false;
}

MeasurementReader::MeasurementData MeasurementReader::readAll(SerialPortSetup* serialPort) {
    unsigned char response[80];
    int rdlen; /* response length */
    int wlen; /* written lenght */
    int fd; /* File Descriptor for Linux OSes*/
    int commandSize = 5;
    
    fd = serialPort->getPortDescriptor();
    
    if (fd < 0) {
        printf("Error opening %s: %s\n", serialPort->getPortName(), strerror(errno));
        return MeasurementData(0.0,0.0,0.0,0.0,0,0);
    }

    /* simple output */
    wlen = write(fd, MeasurementReader::READ_ALL, commandSize);
    if (wlen != commandSize) {
        printf("Error from write: %d, %d\n", wlen, errno);
    }

    tcdrain(fd);    /* delay for output */

    rdlen = read(fd, response, sizeof(response) - 1);

#ifdef DEBUG_OUTPUT    
    if (rdlen > 0) {
        unsigned char   *p;
       
        printf("Read %d:", rdlen);
        for (int i=0; i < rdlen; i++)
            printf(" %02X", response[i]);
        printf("\n");
        if(!isValidResponse(response, rdlen)){
            printf("Checksum Failed!\n");
        }            
    } else if (rdlen < 0) {
        printf("Error from read: %d: %s\n", rdlen, strerror(errno));
    } else {  /* rdlen == 0 */
        printf("Timeout from read\n");
    }
#endif
    
    float co2 = readCo2(response, rdlen);
    float voc = readVoc(response, rdlen);
    float temp = readTemp(response, rdlen);
    float humidity = readHumidity(response, rdlen);
    float pm25 = readPm25(response, rdlen);
    struct timeval tp;
    
    gettimeofday(&tp,NULL);
    
    return MeasurementData(co2,voc,temp,humidity,pm25,tp.tv_sec);
}


/* MeasurementReader::MeasurementData implementation */
MeasurementReader::MeasurementData::MeasurementData(float co2, float voc, float temp, float hum, float pm25, long timestamp) {
    this->co2 = co2;
    this->voc = voc;
    this->temp = temp;
    this->humidity = hum;
    this->pm25 = pm25;
    this->timestamp =timestamp;
}

float MeasurementReader::MeasurementData::getCo2() {
    return co2;
}

float MeasurementReader::MeasurementData::getHumidity() {
    return humidity;
}

float MeasurementReader::MeasurementData::getPm25() {
    return pm25;
}

float MeasurementReader::MeasurementData::getTemp() {
    return temp;
}

float MeasurementReader::MeasurementData::getVoc() {
    return voc;
}

long MeasurementReader::MeasurementData::getTimestamp() {
    return timestamp;
}

unsigned int MeasurementReader::MeasurementData::getDeviceId() {
    return deviceId;
}

void MeasurementReader::MeasurementData::setDeviceId(unsigned int id) {
    deviceId = id;
}



